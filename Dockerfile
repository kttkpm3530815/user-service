FROM openjdk:17-alpine
# Set the working directory in the container
WORKDIR /app
# Copy the executable JAR into the container
COPY target/*.jar ./user-service.jar
# Set environment variables
#ENV REDIS_HOST=redis
#ENV MONGO_HOST=mongo
# Expose port 8080 to the outside world
EXPOSE 8084
CMD ["java", "-jar", "user-service.jar"]