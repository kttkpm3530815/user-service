package com.example.user.controllers;

import com.example.user.User;
import com.example.user.services.JwtService;
import com.example.user.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Slf4j
public class UserController {

    private final UserService userService;
    private final JwtService jwtService;

    @GetMapping("/{id}")
    public User findUserById(@PathVariable String id) {
        log.info("path=/user/{id}");
        log.info("Finding user by id: id={}", id);
        return userService.findById(id);
    }

    @GetMapping("/getUserFromToken")
    public User getUser(HttpServletRequest request) {
        log.info("path=/user/getUserFromToken");
        log.info("Getting user");
        String token = request.getHeader("Authorization").substring(7);
        String userEmail = jwtService.extractUsername(token);
        return userService.findByEmail(userEmail);
    }

    @GetMapping("/getAll")
    public List<User> getAllUsers() {
        log.info("path=/user/getAll");
        log.info("Getting all users");
        return userService.findAll();
    }

}
