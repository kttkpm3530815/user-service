package com.example.user.repositories;

import com.example.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String>{
    Optional<User> findByEmail(String email);
    // fin user by email
    Optional<User> findUserByEmail(String email);
}
